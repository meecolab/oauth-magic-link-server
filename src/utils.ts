import { randomBytes, createHash } from 'crypto';

import { OAuthOptions } from './model';

export async function validateClient(
  options: OAuthOptions,
  clientId: string,
  redirectUri: string
): Promise<boolean> {
  const oauthClient = await options.findClientById(clientId);
  if (!oauthClient) {
    return false;
  }

  return oauthClient.id === clientId && oauthClient.redirectUri === redirectUri;
}

export function randomUrlSafeToken(length: number): string {
  const random = randomBytes(length);
  const hash = createHash('sha256');
  return hash
    .update(random)
    .digest('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');
}
