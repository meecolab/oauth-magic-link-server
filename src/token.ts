import { RequestHandler, Request, Response } from 'express';
import { createHash } from 'crypto';

import {
  OAuthOptions,
  OAuthCode,
  OAuthToken,
  OAuthTokenType,
  BEARER_TOKEN_TYPE
} from './model';
import { randomUrlSafeToken } from './utils';
import { PLAIN_CODE_CHALLENGE_METHOD } from './magicLinkAuthorize';

const AUTH_CODE_GRANT_TYPE = 'authorization_code';
const REFRESH_TOKEN_GRANT_TYPE = 'refresh_token';
const VALID_GRANT_TYPES = [AUTH_CODE_GRANT_TYPE, REFRESH_TOKEN_GRANT_TYPE];

function validateGrantType(grantType: string): boolean {
  return VALID_GRANT_TYPES.includes(grantType);
}

function validateAuthorizationCodeRequestParameters(
  options: OAuthOptions,
  req: Request
): boolean {
  const {
    client_id: clientId,
    redirect_uri: redirectUri,
    code,
    code_verifier: codeVerifier
  } = req.body;

  return (
    clientId &&
    redirectUri &&
    code &&
    ((!options.codeChallengeRequired && !codeVerifier) || codeVerifier)
  );
}

function deriveCodeVerifierChallenge(
  codeVerifier: string,
  codeChallengeMethod: string
): string {
  if (codeChallengeMethod === PLAIN_CODE_CHALLENGE_METHOD) {
    return codeVerifier;
  }

  const hash = createHash('sha256');
  return hash
    .update(codeVerifier)
    .digest('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');
}

function validateAuthCode(
  options: OAuthOptions,
  authCode: OAuthCode,
  clientId: string,
  codeVerifier: string
): boolean {
  return (
    authCode &&
    authCode.clientId === clientId &&
    authCode.expirationTimestamp > Date.now() / 1000 &&
    ((!options.codeChallengeRequired && !authCode.codeChallenge) ||
      deriveCodeVerifierChallenge(
        codeVerifier,
        authCode.codeChallengeMethod
      ) === authCode.codeChallenge)
  );
}

const ACCESS_TOKEN_LENGTH = 32;
const REFRESH_TOKEN_LENGTH = 32;

async function issueTokens(
  options: OAuthOptions,
  clientId: string,
  userId: string,
  issueRefreshToken: boolean,
  res: Response
): Promise<void> {
  const accessToken = randomUrlSafeToken(ACCESS_TOKEN_LENGTH);
  const expirationTimestamp =
    Date.now() / 1000 + options.accessTokenLifeSpanInSeconds;
  const accessTokenObject: OAuthToken = {
    clientId,
    userId,
    expirationTimestamp,
    token: accessToken
  };
  await options.saveToken(OAuthTokenType.AccessToken, accessTokenObject);

  let refreshToken = undefined;
  if (issueRefreshToken) {
    refreshToken = randomUrlSafeToken(REFRESH_TOKEN_LENGTH);
    const refreshExpirationTimestamp =
      Date.now() / 1000 + options.refreshTokenLifeSpanInSeconds;
    const refreshTokenObject: OAuthToken = {
      clientId,
      userId,
      expirationTimestamp: refreshExpirationTimestamp,
      token: refreshToken
    };
    await options.saveToken(OAuthTokenType.RefreshToken, refreshTokenObject);
  }

  res
    .header('Cache-Control', 'no-store')
    .header('Pragma', 'no-cache')
    .json({
      token_type: BEARER_TOKEN_TYPE,
      access_token: accessToken,
      expires_in: parseInt(
        (expirationTimestamp - Date.now() / 1000).toFixed(0)
      ),
      refresh_token: refreshToken
    });
}

async function authorizationCode(
  options: OAuthOptions,
  req: Request,
  res: Response
): Promise<void> {
  if (!validateAuthorizationCodeRequestParameters(options, req)) {
    res.status(400).json({ error: 'invalid_request' });
    return;
  }

  const {
    code,
    client_id: clientId,
    redirect_uri: redirectUri,
    code_verifier: codeVerifier
  } = req.body;

  const oauthClient = await options.findClientById(clientId);
  if (!oauthClient || oauthClient.id !== clientId) {
    res.status(401).json({ error: 'invalid_client' });
    return;
  }

  if (oauthClient.redirectUri !== redirectUri) {
    res.status(400).json({ error: 'invalid_grant' });
    return;
  }

  const authCode = await options.findAuthCode(code);
  if (!validateAuthCode(options, authCode, clientId, codeVerifier)) {
    res.status(400).json({ error: 'invalid_grant' });
    return;
  }

  await options.deleteAuthCode(code);

  await issueTokens(options, clientId, authCode.userId, true, res);
}

function validateRefreshTokenRequestParameters(req: Request): boolean {
  const { client_id: clientId, refresh_token: refreshToken } = req.body;

  return clientId && refreshToken;
}

function validateRefreshToken(
  refreshToken: OAuthToken,
  clientId: string
): boolean {
  return (
    refreshToken &&
    refreshToken.clientId == clientId &&
    refreshToken.expirationTimestamp > Date.now() / 1000
  );
}

function refreshTokenNeedsRefresh(
  options: OAuthOptions,
  refreshToken: OAuthToken
): boolean {
  return (
    options.alwaysIssueNewRefreshToken ||
    refreshToken.expirationTimestamp +
      options.issueNewRefreshTokenEarlyInSeconds >
      Date.now() / 1000
  );
}

async function refreshToken(
  options: OAuthOptions,
  req: Request,
  res: Response
): Promise<void> {
  if (!validateRefreshTokenRequestParameters(req)) {
    res.status(400).json({ error: 'invalid_request' });
    return;
  }

  const { client_id: clientId, refresh_token: refreshToken } = req.body;

  const oauthClient = await options.findClientById(clientId);
  if (!oauthClient || oauthClient.id !== clientId) {
    res.status(401).json({ error: 'invalid_client' });
    return;
  }

  const refreshTokenObject = await options.findToken(
    OAuthTokenType.RefreshToken,
    refreshToken
  );
  if (!validateRefreshToken(refreshTokenObject, clientId)) {
    res.status(400).json({ error: 'invalid_grant' });
    return;
  }

  const issueNewRefreshToken = refreshTokenNeedsRefresh(
    options,
    refreshTokenObject
  );
  if (issueNewRefreshToken) {
    await options.deleteToken(OAuthTokenType.RefreshToken, refreshToken);
  }

  await issueTokens(
    options,
    clientId,
    refreshTokenObject.userId,
    issueNewRefreshToken,
    res
  );
}

export function token(options: OAuthOptions): RequestHandler {
  return async (req: Request, res: Response): Promise<void> => {
    const { grant_type: grantType } = req.body;

    try {
      if (!validateGrantType(grantType)) {
        res.status(400).json({ error: 'unsupported_grant_type' });
        return;
      }

      if (grantType === AUTH_CODE_GRANT_TYPE) {
        await authorizationCode(options, req, res);
      } else {
        await refreshToken(options, req, res);
      }
    } catch (error) {
      res.status(500).json({ error: 'server_error' });
    }
  };
}
