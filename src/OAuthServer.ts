import { RequestHandler } from 'express';

import { OAuthOptions } from './model';
import { magicLinkAuthorize } from './magicLinkAuthorize';
import { token } from './token';
import { requireAuthorization } from './requireAuthorization';

const DEFAULT_AUTH_CODE_LIFE_SPAN_IN_SECONDS = 60; //1 min
const DEFAULT_ACCESS_TOKEN_LIFE_SPAN_IN_SECONDS = 3600; // 1 h
const DEFAULT_REFRESH_TOKEN_LIFE_SPAN_IN_SECONDS = 1314000; // 1 year

const DEFAULT_OPTIONS: Partial<OAuthOptions> = {
  stateRequired: true,

  codeChallengeRequired: true,
  plainCodeChallengeAllowed: false,

  authCodeLifeSpanInSeconds: DEFAULT_AUTH_CODE_LIFE_SPAN_IN_SECONDS,

  handleAuthCodeSuccessResponse: true,

  alwaysIssueNewRefreshToken: true,

  accessTokenLifeSpanInSeconds: DEFAULT_ACCESS_TOKEN_LIFE_SPAN_IN_SECONDS,
  refreshTokenLifeSpanInSeconds: DEFAULT_REFRESH_TOKEN_LIFE_SPAN_IN_SECONDS
};

export class OAuthServer {
  private readonly options: OAuthOptions;

  constructor(options: Partial<OAuthOptions>) {
    this.options = { ...DEFAULT_OPTIONS, ...options } as OAuthOptions;
    // TODO: validate
  }

  magicLinkAuthorize(): RequestHandler {
    return magicLinkAuthorize(this.options);
  }

  token(): RequestHandler {
    return token(this.options);
  }

  requireAuthorization(): RequestHandler {
    return requireAuthorization(this.options);
  }
}
