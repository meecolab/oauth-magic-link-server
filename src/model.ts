export interface OAuthClient {
  id: string;
  redirectUri: string;
}

export interface User {
  id: string;
  email: string;
}

export interface OAuthCode {
  token: string;
  clientId: string;
  userId: string;
  codeChallenge: string;
  codeChallengeMethod: string;
  expirationTimestamp: number;
}

export enum OAuthTokenType {
  AccessToken,
  RefreshToken
}

export interface OAuthToken {
  token: string;
  clientId: string;
  userId: string;
  expirationTimestamp: number;
}

export const BEARER_TOKEN_TYPE = 'Bearer';

export type MagicLinkOptions = {
  redirectUri: string;
  code: string;
  state: string;
};

export type OAuthOptions = {
  stateRequired: boolean;

  codeChallengeRequired: boolean;
  plainCodeChallengeAllowed: boolean;

  authCodeLifeSpanInSeconds: number;

  handleAuthCodeSuccessResponse: boolean;

  accessTokenLifeSpanInSeconds: number;
  refreshTokenLifeSpanInSeconds: number;

  alwaysIssueNewRefreshToken: boolean;
  issueNewRefreshTokenEarlyInSeconds: number;

  findClientById: (clientId: string) => Promise<OAuthClient | undefined>;

  findOrCreateUserWithEmail: (email: string) => Promise<User>;
  findUserById: (id: string) => Promise<User>;

  saveAuthCode: (code: OAuthCode) => Promise<void>;
  findAuthCode: (code: string) => Promise<OAuthCode | undefined>;
  deleteAuthCode: (code: string) => Promise<void>;

  sendMagicLink: (
    emailAddress: string,
    magicLinkOptions: MagicLinkOptions
  ) => Promise<void>;

  saveToken: (type: OAuthTokenType, token: OAuthToken) => Promise<void>;
  findToken: (
    type: OAuthTokenType,
    token: string
  ) => Promise<OAuthToken | undefined>;
  deleteToken: (type: OAuthTokenType, token: string) => Promise<void>;
};
