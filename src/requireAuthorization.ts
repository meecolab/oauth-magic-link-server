import { RequestHandler, Request, Response, NextFunction } from 'express';

import { OAuthOptions, OAuthTokenType, BEARER_TOKEN_TYPE } from './model';

export function requireAuthorization(options: OAuthOptions): RequestHandler {
  return async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { authorization } = req.headers;

      if (!authorization) {
        res
          .status(400)
          .header(
            'WWW-Authenticate',
            'Bearer realm="/",error="invalid_request"'
          )
          .send('');
        return;
      }

      const [type, token] = authorization.split(' ');
      if (!type || !token) {
        res
          .status(400)
          .header(
            'WWW-Authenticate',
            'Bearer realm="/",error="invalid_request"'
          )
          .send('');
        return;
      }

      if (type !== BEARER_TOKEN_TYPE) {
        res
          .status(401)
          .header('WWW-Authenticate', 'Bearer realm="/",error="invalid_token"')
          .send('');
        return;
      }

      const accessToken = await options.findToken(
        OAuthTokenType.AccessToken,
        token
      );
      if (
        !accessToken ||
        accessToken.token !== token ||
        accessToken.expirationTimestamp < Date.now() / 1000
      ) {
        res
          .status(401)
          .header('WWW-Authenticate', 'Bearer realm="/",error="invalid_token"')
          .send('');
        return;
      }

      const user = await options.findUserById(accessToken.userId);
      res.locals.user = user;

      next();
    } catch (error) {
      res
        .status(500)
        .header('WWW-Authenticate', 'Bearer realm="/",error="server_error"')
        .send('');
    }
  };
}
