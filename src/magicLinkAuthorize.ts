import { RequestHandler, Request, Response, NextFunction } from 'express';

import { OAuthOptions, OAuthCode } from './model';
import { randomUrlSafeToken, validateClient } from './utils';

function validateRequestParameters(
  options: OAuthOptions,
  req: Request
): boolean {
  const {
    response_type: responseType,
    client_id: clientId,
    redirect_uri: redirectUri,
    state,
    code_challenge: codeChallenge,
    code_challenge_method: codeChallengeMethod,
    email
  } = req.body;

  return (
    responseType &&
    clientId &&
    redirectUri &&
    (!options.stateRequired || state) &&
    (!options.codeChallengeRequired ||
      (codeChallenge && codeChallengeMethod)) &&
    email
  );
}

const VALID_RESPONSE_TYPE = 'code';

function validateResponseType(responseType: string): boolean {
  return responseType === VALID_RESPONSE_TYPE;
}

export const PLAIN_CODE_CHALLENGE_METHOD = 'plain';
const SHA256_CODE_CHALLENGE_METHOD = 'S256';

function validateCodeChallenge(
  options: OAuthOptions,
  codeChallenge: string,
  codeChallengeMethod: string
): boolean {
  if (
    !options.codeChallengeRequired &&
    !codeChallenge &&
    !codeChallengeMethod
  ) {
    return true;
  }

  if (
    options.codeChallengeRequired &&
    (!codeChallenge || !codeChallengeMethod)
  ) {
    return false;
  }

  if (
    options.plainCodeChallengeAllowed &&
    codeChallengeMethod === PLAIN_CODE_CHALLENGE_METHOD
  ) {
    return true;
  }

  return codeChallengeMethod === SHA256_CODE_CHALLENGE_METHOD;
}

const AUTH_CODE_LENGTH = 32;

export function magicLinkAuthorize(options: OAuthOptions): RequestHandler {
  return async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      if (!validateRequestParameters(options, req)) {
        res.status(400).json({ error: 'invalid_request' });
        return;
      }

      const {
        response_type: responseType,
        client_id: clientId,
        redirect_uri: redirectUri,
        state,
        code_challenge: codeChallenge,
        code_challenge_method: codeChallengeMethod,
        email
      } = req.body;

      if (!validateResponseType(responseType)) {
        res.status(400).json({ error: 'unsupported_response_type' });
        return;
      }

      if (!validateCodeChallenge(options, codeChallenge, codeChallengeMethod)) {
        res.status(400).json({ error: 'invalid_request' });
        return;
      }

      if (!(await validateClient(options, clientId, redirectUri))) {
        res.status(400).json({ error: 'unauthorized_client' });
        return;
      }

      const user = await options.findOrCreateUserWithEmail(email);
      if (!user) {
        throw new Error(
          'findOrCreateUserWithEmail has to return a user or throw an exception by itself'
        );
      }

      const token = randomUrlSafeToken(AUTH_CODE_LENGTH);
      const expirationTimestamp =
        Date.now() / 1000 + options.authCodeLifeSpanInSeconds;
      const code: OAuthCode = {
        token,
        clientId,
        codeChallenge,
        codeChallengeMethod,
        expirationTimestamp,
        userId: user.id
      };
      await options.saveAuthCode(code);

      const magicLinkOptions = {
        redirectUri,
        code: code.token,
        state
      };
      await options.sendMagicLink(email, magicLinkOptions);

      if (options.handleAuthCodeSuccessResponse) {
        res.json({ ok: true });
      } else {
        next();
      }
    } catch (error) {
      res.status(500).json({ error: 'server_error' });
    }
  };
}
