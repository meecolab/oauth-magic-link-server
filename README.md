# MeecoLabs's OAuth Magic Link Server

A very simple implementation of an OAuth 2 server that only supports authentication via sending a magic link.
