import { OAuthOptions, OAuthServer } from '../src';
import * as magicLinkAuthorize from '../src/magicLinkAuthorize';
import * as token from '../src/token';
import * as requireAuthorization from '../src/requireAuthorization';

describe('OAuthServer', () => {
  let server: OAuthServer;

  beforeEach(() => {
    const options = {} as OAuthOptions;
    server = new OAuthServer(options);
  });

  describe('magicLinkAuthorize', () => {
    let authorizeSpy: jest.SpyInstance;

    beforeEach(() => {
      authorizeSpy = jest.spyOn(magicLinkAuthorize, 'magicLinkAuthorize');
    });

    afterEach(() => {
      authorizeSpy.mockRestore();
    });

    it('returns a handler', () => {
      const result = server.magicLinkAuthorize();
      expect(result).toBeDefined();
      expect(authorizeSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('token', () => {
    let tokenSpy: jest.SpyInstance;

    beforeEach(() => {
      tokenSpy = jest.spyOn(token, 'token');
    });

    afterEach(() => {
      tokenSpy.mockRestore();
    });

    it('returns a handler', () => {
      const result = server.token();
      expect(result).toBeDefined();
      expect(tokenSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('requireAuthorization', () => {
    let requireAuthorizationSpy: jest.SpyInstance;

    beforeEach(() => {
      requireAuthorizationSpy = jest.spyOn(
        requireAuthorization,
        'requireAuthorization'
      );
    });

    afterEach(() => {
      requireAuthorizationSpy.mockRestore();
    });

    it('returns a handler', () => {
      const result = server.requireAuthorization();
      expect(result).toBeDefined();
      expect(requireAuthorizationSpy).toHaveBeenCalledTimes(1);
    });
  });
});
