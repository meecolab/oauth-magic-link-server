import { RequestHandler } from 'express';

import { OAuthOptions } from '../src';
import { token } from '../src/token';
import * as utils from '../src/utils';
import { mockRequest, mockResponse } from './__utils__/mockExpress';

describe('token', () => {
  describe('create', () => {
    let handler: RequestHandler;

    beforeEach(() => {
      const options = {} as OAuthOptions;
      handler = token(options);
    });

    it('creates a handler function', () => {
      expect(handler).toBeDefined();
    });
  });

  describe('handler', () => {
    const req = mockRequest();
    const res = mockResponse();

    const findClientById = jest.fn();
    const findOrCreateUserWithEmail = jest.fn();
    const saveAuthCode = jest.fn();
    const sendMagicLink = jest.fn();
    const findAuthCode = jest.fn();
    const deleteAuthCode = jest.fn();
    const saveToken = jest.fn();
    const findToken = jest.fn();
    const deleteToken = jest.fn();

    let randomUrlSafeTokenSpy: jest.SpyInstance;
    let dateNowSpy: jest.SpyInstance;

    let options: OAuthOptions;

    beforeEach(() => {
      options = ({
        findClientById,
        findOrCreateUserWithEmail,
        saveAuthCode,
        sendMagicLink,
        findAuthCode,
        deleteAuthCode,
        saveToken,
        findToken,
        deleteToken
      } as unknown) as OAuthOptions;

      randomUrlSafeTokenSpy = jest
        .spyOn(utils, 'randomUrlSafeToken')
        .mockReturnValue('mock');
      dateNowSpy = jest.spyOn(Date, 'now').mockReturnValue(5000);
    });

    afterEach(() => {
      ((res.header as unknown) as jest.SpyInstance).mockClear();
      ((res.status as unknown) as jest.SpyInstance).mockClear();
      ((res.json as unknown) as jest.SpyInstance).mockClear();

      findClientById.mockReset();
      findOrCreateUserWithEmail.mockReset();
      saveAuthCode.mockReset();
      sendMagicLink.mockReset();
      findAuthCode.mockReset();
      deleteAuthCode.mockReset();
      saveToken.mockReset();
      findToken.mockReset();
      deleteToken.mockReset();

      randomUrlSafeTokenSpy.mockRestore();
      dateNowSpy.mockRestore();
    });

    describe('failure', () => {
      it('has an unsupported grant type', async () => {
        req.body = {
          grant_type: 'wrong'
        };
        const handler = token(options);
        const result = handler(req, res, undefined);

        await expect(result).resolves.toBeUndefined();

        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'unsupported_grant_type'
        });
      });
    });

    describe('authorizationCode', () => {
      beforeEach(async () => {
        req.body = {
          grant_type: 'authorization_code',
          client_id: 'mock',
          redirect_uri: 'mock',
          code_verifier: 'mock',
          code: 'mock'
        };

        findClientById.mockReturnValue({
          id: 'mock',
          redirectUri: 'mock'
        });
        findAuthCode.mockReturnValue({
          token: 'mock',
          clientId: 'mock',
          codeChallenge: 'mock',
          codeChallengeMethod: 'plain',
          expirationTimestamp: 10,
          userId: 'mock'
        });
      });

      describe('success', () => {
        it('responds with valid request', async () => {
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).toHaveBeenCalledTimes(1);
          expect(deleteAuthCode).toHaveBeenCalledTimes(1);
          expect(saveToken).toHaveBeenCalledTimes(2);
          expect(res.header).toHaveBeenCalledTimes(2);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            token_type: 'Bearer',
            access_token: 'mock',
            expires_in: expect.any(Number),
            refresh_token: 'mock'
          });
        });
      });

      describe('failure', () => {
        it('fails to save token', async () => {
          saveToken.mockRejectedValue('mock');

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).toHaveBeenCalledTimes(1);
          expect(deleteAuthCode).toHaveBeenCalledTimes(1);
          expect(saveToken).toHaveBeenCalledTimes(1);
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(500);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'server_error'
          });
        });

        it('fails to delete auth code', async () => {
          deleteAuthCode.mockRejectedValue('mock');

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).toHaveBeenCalledTimes(1);
          expect(deleteAuthCode).toHaveBeenCalledTimes(1);
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(500);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'server_error'
          });
        });

        it('fails to find auth code', async () => {
          findAuthCode.mockRejectedValue('mock');

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).toHaveBeenCalledTimes(1);
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(500);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'server_error'
          });
        });

        it('returns with empty auth code', async () => {
          findAuthCode.mockReturnValue(undefined);

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).toHaveBeenCalledTimes(1);
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_grant'
          });
        });

        it('returns with wrong auth code client id', async () => {
          findAuthCode.mockReturnValue({
            token: 'mock',
            clientId: 'wrong',
            codeChallenge: 'mock',
            codeChallengeMethod: 'plain',
            expirationTimestamp: 10,
            userId: 'mock'
          });

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).toHaveBeenCalledTimes(1);
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_grant'
          });
        });

        it('returns with wrong auth code expiration date', async () => {
          findAuthCode.mockReturnValue({
            token: 'mock',
            clientId: 'mock',
            codeChallenge: 'mock',
            codeChallengeMethod: 'plain',
            expirationTimestamp: 1,
            userId: 'mock'
          });

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).toHaveBeenCalledTimes(1);
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_grant'
          });
        });

        it('returns with wrong auth code expiration date', async () => {
          findAuthCode.mockReturnValue({
            token: 'mock',
            clientId: 'mock',
            codeChallenge: 'mock',
            codeChallengeMethod: 'S256',
            expirationTimestamp: 10,
            userId: 'mock'
          });

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).toHaveBeenCalledTimes(1);
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_grant'
          });
        });

        it('returns with empty client', async () => {
          findClientById.mockRejectedValue('mock');

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).not.toHaveBeenCalled();
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(500);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'server_error'
          });
        });

        it('returns with wrong client redirect uri', async () => {
          findClientById.mockReturnValue({
            id: 'mock',
            redirectUri: 'wrong'
          });

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).not.toHaveBeenCalled();
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_grant'
          });
        });

        it('returns with wrong client id', async () => {
          findClientById.mockReturnValue({
            id: 'wrong',
            redirectUri: 'mock'
          });

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).not.toHaveBeenCalled();
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(401);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_client'
          });
        });

        it('returns with empty client', async () => {
          findClientById.mockReturnValue(undefined);

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findAuthCode).not.toHaveBeenCalled();
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(401);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_client'
          });
        });

        it('returns with missing client id', async () => {
          req.body.client_id = undefined;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).not.toHaveBeenCalled();
          expect(findAuthCode).not.toHaveBeenCalled();
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_request'
          });
        });

        it('returns with missing redirect uri', async () => {
          req.body.redirect_uri = undefined;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).not.toHaveBeenCalled();
          expect(findAuthCode).not.toHaveBeenCalled();
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_request'
          });
        });

        it('returns with missing code', async () => {
          req.body.code = undefined;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).not.toHaveBeenCalled();
          expect(findAuthCode).not.toHaveBeenCalled();
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_request'
          });
        });

        it('returns with missing code verifier', async () => {
          req.body.code_verifier = undefined;
          options.codeChallengeRequired = true;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).not.toHaveBeenCalled();
          expect(findAuthCode).not.toHaveBeenCalled();
          expect(deleteAuthCode).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_request'
          });
        });
      });
    });

    describe('refreshToken', () => {
      beforeEach(async () => {
        req.body = {
          grant_type: 'refresh_token',
          client_id: 'mock',
          refresh_token: 'mock'
        };

        findClientById.mockReturnValue({
          id: 'mock',
          redirectUri: 'mock'
        });
        findToken.mockReturnValue({
          clientId: 'mock',
          userId: 'mock',
          expirationTimestamp: 10,
          token: 'mock'
        });
      });

      describe('success', () => {
        it('responds with valid request', async () => {
          options.alwaysIssueNewRefreshToken = true;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).toHaveBeenCalledTimes(1);
          expect(deleteToken).toHaveBeenCalledTimes(1);
          expect(saveToken).toHaveBeenCalledTimes(2);
          expect(res.header).toHaveBeenCalledTimes(2);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            token_type: 'Bearer',
            access_token: 'mock',
            expires_in: expect.any(Number),
            refresh_token: 'mock'
          });
        });

        it('responds with valid request without new refresh token', async () => {
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).toHaveBeenCalledTimes(1);
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).toHaveBeenCalledTimes(1);
          expect(res.header).toHaveBeenCalledTimes(2);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            token_type: 'Bearer',
            access_token: 'mock',
            expires_in: expect.any(Number)
          });
        });
      });

      describe('failure', () => {
        it('fails saving token', async () => {
          saveToken.mockRejectedValue('mock');
          options.alwaysIssueNewRefreshToken = true;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).toHaveBeenCalledTimes(1);
          expect(deleteToken).toHaveBeenCalledTimes(1);
          expect(saveToken).toHaveBeenCalledTimes(1);
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(500);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'server_error'
          });
        });

        it('fails deleting old refresh token', async () => {
          deleteToken.mockRejectedValue('mock');
          options.alwaysIssueNewRefreshToken = true;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).toHaveBeenCalledTimes(1);
          expect(deleteToken).toHaveBeenCalledTimes(1);
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(500);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'server_error'
          });
        });

        it('fails with find refresh token', async () => {
          findToken.mockRejectedValue('mock');
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).toHaveBeenCalledTimes(1);
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(500);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'server_error'
          });
        });

        it('returns with empty refresh token', async () => {
          findToken.mockReturnValue(undefined);
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).toHaveBeenCalledTimes(1);
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_grant'
          });
        });

        it('returns with wrong refresh token client id', async () => {
          findToken.mockReturnValue({
            clientId: 'wrong',
            expirationTimestamp: 10
          });
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).toHaveBeenCalledTimes(1);
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_grant'
          });
        });

        it('returns with expired refresh token', async () => {
          findToken.mockReturnValue({
            clientId: 'mock',
            expirationTimestamp: 5
          });

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).toHaveBeenCalledTimes(1);
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_grant'
          });
        });

        it('fails with find client', async () => {
          findClientById.mockRejectedValue('mock');

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).not.toHaveBeenCalled();
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(500);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'server_error'
          });
        });

        it('returns with empty client', async () => {
          findClientById.mockReturnValue(undefined);

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).not.toHaveBeenCalled();
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(401);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_client'
          });
        });

        it('returns with wrong client id', async () => {
          findClientById.mockReturnValue({ id: 'wrong' });

          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).toHaveBeenCalledTimes(1);
          expect(findToken).not.toHaveBeenCalled();
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(401);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_client'
          });
        });

        it('returns with missing client id', async () => {
          req.body.client_id = undefined;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).not.toHaveBeenCalled();
          expect(findToken).not.toHaveBeenCalled();
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_request'
          });
        });

        it('returns with missing refresh token', async () => {
          req.body.refresh_token = undefined;
          const handler = token(options);
          const result = handler(req, res, undefined);

          await expect(result).resolves.toBeUndefined();

          expect(findClientById).not.toHaveBeenCalled();
          expect(findToken).not.toHaveBeenCalled();
          expect(deleteToken).not.toHaveBeenCalled();
          expect(saveToken).not.toHaveBeenCalled();
          expect(res.header).not.toHaveBeenCalled();
          expect(res.status).toHaveBeenCalledTimes(1);
          expect(res.status).toHaveBeenCalledWith(400);
          expect(res.json).toHaveBeenCalledTimes(1);
          expect(res.json).toHaveBeenCalledWith({
            error: 'invalid_request'
          });
        });
      });
    });
  });
});
