import { RequestHandler, Request, Response } from 'express';

import { OAuthOptions } from '../src';
import { requireAuthorization } from '../src/requireAuthorization';
import { mockRequest, mockResponse } from './__utils__/mockExpress';

describe('requireAuthorization', () => {
  describe('create', () => {
    let handler: RequestHandler;

    beforeEach(() => {
      const options = {} as OAuthOptions;
      handler = requireAuthorization(options);
    });

    it('creates a handler function', () => {
      expect(handler).toBeDefined();
    });
  });

  describe('handler', () => {
    const req = mockRequest();
    const res = mockResponse();
    const next = jest.fn();

    const findToken = jest.fn();

    let dateNowSpy: jest.SpyInstance;

    let options: OAuthOptions;

    beforeEach(() => {
      options = ({
        findToken
      } as unknown) as OAuthOptions;

      dateNowSpy = jest.spyOn(Date, 'now').mockReturnValue(5000);
    });

    afterEach(() => {
      ((res.status as unknown) as jest.SpyInstance).mockClear();
      ((res.header as unknown) as jest.SpyInstance).mockClear();
      ((res.send as unknown) as jest.SpyInstance).mockClear();
      next.mockReset();

      findToken.mockReset();

      dateNowSpy.mockRestore();
    });

    describe('success', () => {
      beforeEach(async () => {
        req.headers = {
          authorization: 'Bearer mock'
        };

        findToken.mockReturnValue({
          clientId: 'mock',
          userId: 'mock',
          expirationTimestamp: 10,
          token: 'mock'
        });
      });

      it('responds with valid request', async () => {
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).toHaveBeenCalledTimes(1);
        expect(res.send).not.toHaveBeenCalled();
        expect(next).toHaveBeenCalledTimes(1);
      });
    });

    describe('failure', () => {
      beforeEach(async () => {
        req.headers = {
          authorization: 'Bearer mock'
        };
      });

      it('returns with expired token', async () => {
        findToken.mockReturnValue({
          clientId: 'mock',
          userId: 'mock',
          expirationTimestamp: 1,
          token: 'mock'
        });
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith('');
        expect(res.header).toHaveBeenCalledTimes(1);
        expect(res.header).toHaveBeenCalledWith(
          'WWW-Authenticate',
          'Bearer realm="/",error="invalid_token"'
        );
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with mismatching token', async () => {
        findToken.mockReturnValue({
          clientId: 'mock',
          userId: 'mock',
          expirationTimestamp: 10,
          token: 'wrong'
        });
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith('');
        expect(res.header).toHaveBeenCalledTimes(1);
        expect(res.header).toHaveBeenCalledWith(
          'WWW-Authenticate',
          'Bearer realm="/",error="invalid_token"'
        );
        expect(next).not.toHaveBeenCalled();
      });

      it('fails with find token', async () => {
        findToken.mockRejectedValue('mock');
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith('');
        expect(res.header).toHaveBeenCalledTimes(1);
        expect(res.header).toHaveBeenCalledWith(
          'WWW-Authenticate',
          'Bearer realm="/",error="server_error"'
        );
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with empty token', async () => {
        findToken.mockReturnValue(undefined);
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith('');
        expect(res.header).toHaveBeenCalledTimes(1);
        expect(res.header).toHaveBeenCalledWith(
          'WWW-Authenticate',
          'Bearer realm="/",error="invalid_token"'
        );
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with invalid authorization type', async () => {
        req.headers.authorization = 'Bearing mock';
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith('');
        expect(res.header).toHaveBeenCalledTimes(1);
        expect(res.header).toHaveBeenCalledWith(
          'WWW-Authenticate',
          'Bearer realm="/",error="invalid_token"'
        );
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing token', async () => {
        req.headers.authorization = 'Bearer ';
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith('');
        expect(res.header).toHaveBeenCalledTimes(1);
        expect(res.header).toHaveBeenCalledWith(
          'WWW-Authenticate',
          'Bearer realm="/",error="invalid_request"'
        );
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing token type', async () => {
        req.headers.authorization = ' mock';
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith('');
        expect(res.header).toHaveBeenCalledTimes(1);
        expect(res.header).toHaveBeenCalledWith(
          'WWW-Authenticate',
          'Bearer realm="/",error="invalid_request"'
        );
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing authorization', async () => {
        req.headers.authorization = '';
        const handler = requireAuthorization(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findToken).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith('');
        expect(res.header).toHaveBeenCalledTimes(1);
        expect(res.header).toHaveBeenCalledWith(
          'WWW-Authenticate',
          'Bearer realm="/",error="invalid_request"'
        );
        expect(next).not.toHaveBeenCalled();
      });
    });
  });
});
