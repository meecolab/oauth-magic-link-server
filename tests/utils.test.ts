import crypto from 'crypto';

import { validateClient, randomUrlSafeToken } from '../src/utils';
import { OAuthOptions } from '../src';

describe('utils', () => {
  describe('validateClient', () => {
    const findClientById = jest.fn();

    let options: OAuthOptions;

    beforeEach(() => {
      options = ({
        findClientById
      } as unknown) as OAuthOptions;
    });

    afterEach(() => {
      findClientById.mockReset();
    });

    it('fails finding a client', async () => {
      findClientById.mockRejectedValue('mock');

      const result = validateClient(options, 'mock', 'mock');

      await expect(result).rejects.toBeDefined();
      await expect(result).rejects.toBe('mock');
    });

    it('returns no client', async () => {
      findClientById.mockReturnValue(undefined);

      const result = validateClient(options, 'mock', 'mock');

      await expect(result).resolves.toBeDefined();
      await expect(result).resolves.toBe(false);
    });

    it('does not match client id', async () => {
      findClientById.mockReturnValue({
        id: 'wrong',
        redirectUri: 'mock'
      });

      const result = validateClient(options, 'mock', 'mock');

      await expect(result).resolves.toBeDefined();
      await expect(result).resolves.toBe(false);
    });

    it('does not match redirect id', async () => {
      findClientById.mockReturnValue({
        id: 'mock',
        redirectUri: 'wrong'
      });

      const result = validateClient(options, 'mock', 'mock');

      await expect(result).resolves.toBeDefined();
      await expect(result).resolves.toBe(false);
    });

    it('validates client', async () => {
      findClientById.mockReturnValue({
        id: 'mock',
        redirectUri: 'mock'
      });

      const result = validateClient(options, 'mock', 'mock');

      await expect(result).resolves.toBeDefined();
      await expect(result).resolves.toBe(true);
    });
  });

  describe('randomUrlSafeToken', () => {
    it('returns a hash', () => {
      const randomBytesSpy = jest
        .spyOn(crypto, 'randomBytes')
        .mockImplementation(() => {
          return Buffer.from('mock');
        });

      const result = randomUrlSafeToken(32);

      expect(result).toBeDefined();
      expect(result).toBe('7IZP6ZtTlwS4hyrFkQZ-8i2Dao2UIIfy26J0swHr5uU');
      expect(randomBytesSpy).toHaveBeenCalled();
    });
  });
});
