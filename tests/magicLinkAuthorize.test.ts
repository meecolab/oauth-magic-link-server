import { RequestHandler, Request, Response } from 'express';

import { magicLinkAuthorize } from '../src/magicLinkAuthorize';
import { OAuthOptions } from '../src';
import { mockRequest, mockResponse } from './__utils__/mockExpress';

describe('magicLinkAuthorize', () => {
  describe('create', () => {
    let handler: RequestHandler;

    beforeEach(() => {
      const options = {} as OAuthOptions;
      handler = magicLinkAuthorize(options);
    });

    it('creates a handler function', () => {
      expect(handler).toBeDefined();
    });
  });

  describe('handler', () => {
    const req = mockRequest();
    const res = mockResponse();
    const next = jest.fn();

    const findClientById = jest.fn();
    const findOrCreateUserWithEmail = jest.fn();
    const saveAuthCode = jest.fn();
    const sendMagicLink = jest.fn();
    const findAuthCode = jest.fn();
    const deleteAuthCode = jest.fn();
    const saveToken = jest.fn();
    const findToken = jest.fn();
    const deleteToken = jest.fn();

    let options: OAuthOptions;

    beforeEach(() => {
      options = ({
        findClientById,
        findOrCreateUserWithEmail,
        saveAuthCode,
        sendMagicLink,
        findAuthCode,
        deleteAuthCode,
        saveToken,
        findToken,
        deleteToken
      } as unknown) as OAuthOptions;
    });

    afterEach(() => {
      ((res.status as unknown) as jest.SpyInstance).mockClear();
      ((res.json as unknown) as jest.SpyInstance).mockClear();
      next.mockReset();

      findClientById.mockReset();
      findOrCreateUserWithEmail.mockReset();
      saveAuthCode.mockReset();
      sendMagicLink.mockReset();
      findAuthCode.mockReset();
      deleteAuthCode.mockReset();
      saveToken.mockReset();
      findToken.mockReset();
      deleteToken.mockReset();
    });

    describe('success', () => {
      beforeEach(async () => {
        req.body = {
          response_type: 'code',
          client_id: 'mock',
          redirect_uri: 'mock',
          state: 'mock',
          code_challenge: 'mock',
          code_challenge_method: 'plain',
          email: 'mock'
        };

        options.plainCodeChallengeAllowed = true;
        findClientById.mockReturnValue({
          id: 'mock',
          redirectUri: 'mock'
        });
        findOrCreateUserWithEmail.mockReturnValue({
          id: 'mock',
          email: 'mock'
        });
      });

      it('responds with valid request', async () => {
        options.handleAuthCodeSuccessResponse = true;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).toHaveBeenCalledTimes(1);
        expect(saveAuthCode).toHaveBeenCalledTimes(1);
        expect(sendMagicLink).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ ok: true });
        expect(next).not.toHaveBeenCalled();
      });

      it('calls next with valid request', async () => {
        options.handleAuthCodeSuccessResponse = false;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).toHaveBeenCalledTimes(1);
        expect(saveAuthCode).toHaveBeenCalledTimes(1);
        expect(sendMagicLink).toHaveBeenCalledTimes(1);
        expect(res.json).not.toHaveBeenCalled();
        expect(next).toHaveBeenCalledTimes(1);
      });
    });

    describe('failure', () => {
      beforeEach(async () => {
        req.body = {
          response_type: 'code',
          client_id: 'mock',
          redirect_uri: 'mock',
          state: 'mock',
          code_challenge: 'mock',
          code_challenge_method: 'plain',
          email: 'mock'
        };

        options.plainCodeChallengeAllowed = true;
        findClientById.mockReturnValue({
          id: 'mock',
          redirectUri: 'mock'
        });
        findOrCreateUserWithEmail.mockReturnValue({
          id: 'mock',
          email: 'mock'
        });
      });

      it('fails to send the magic link', async () => {
        sendMagicLink.mockRejectedValue('mock');
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).toHaveBeenCalledTimes(1);
        expect(saveAuthCode).toHaveBeenCalledTimes(1);
        expect(sendMagicLink).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'server_error' });
        expect(next).not.toHaveBeenCalled();
      });

      it('fails to save the auth code', async () => {
        saveAuthCode.mockRejectedValue('mock');
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).toHaveBeenCalledTimes(1);
        expect(saveAuthCode).toHaveBeenCalledTimes(1);
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'server_error' });
        expect(next).not.toHaveBeenCalled();
      });

      it('fails to find or create user', async () => {
        findOrCreateUserWithEmail.mockRejectedValue('mock');
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).toHaveBeenCalledTimes(1);
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'server_error' });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns empty user', async () => {
        findOrCreateUserWithEmail.mockReturnValue(undefined);
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).toHaveBeenCalledTimes(1);
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'server_error' });
        expect(next).not.toHaveBeenCalled();
      });

      it('fails find oauth client', async () => {
        findClientById.mockRejectedValue('mock');
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'server_error' });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns empty oauth client', async () => {
        findClientById.mockReturnValue(undefined);
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'unauthorized_client' });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns a wrong oauth client id', async () => {
        findClientById.mockReturnValue({ id: 'wrong' });
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'unauthorized_client' });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns a wrong oauth client redirect uri', async () => {
        findClientById.mockReturnValue({
          id: 'mock',
          redirectUri: 'wrong'
        });
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).toHaveBeenCalledTimes(1);
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'unauthorized_client' });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns required sha 256 code challenge', async () => {
        options.codeChallengeRequired = true;
        options.plainCodeChallengeAllowed = false;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ error: 'invalid_request' });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns not required code challenge', async () => {
        options.codeChallengeRequired = false;
        req.body.code_challenge = undefined;
        req.body.code_challenge_method = undefined;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(next).toHaveBeenCalledTimes(1);
      });

      it('returns with invalid response type', async () => {
        req.body.response_type = 'invalid';
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'unsupported_response_type'
        });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing response type', async () => {
        req.body.response_type = undefined;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'invalid_request'
        });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing client id', async () => {
        req.body.client_id = undefined;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'invalid_request'
        });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing redirect uri', async () => {
        req.body.redirect_uri = undefined;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'invalid_request'
        });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing state', async () => {
        req.body.state = undefined;
        options.stateRequired = true;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'invalid_request'
        });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing code challenge', async () => {
        req.body.code_challenge = undefined;
        options.codeChallengeRequired = true;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'invalid_request'
        });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing code challenge method', async () => {
        req.body.code_challenge_method = undefined;
        options.codeChallengeRequired = true;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'invalid_request'
        });
        expect(next).not.toHaveBeenCalled();
      });

      it('returns with missing email', async () => {
        req.body.email = undefined;
        const handler = magicLinkAuthorize(options);
        const result = handler(req, res, next);

        await expect(result).resolves.toBeUndefined();

        expect(findClientById).not.toHaveBeenCalled();
        expect(findOrCreateUserWithEmail).not.toHaveBeenCalled();
        expect(saveAuthCode).not.toHaveBeenCalled();
        expect(sendMagicLink).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
          error: 'invalid_request'
        });
        expect(next).not.toHaveBeenCalled();
      });
    });
  });
});
